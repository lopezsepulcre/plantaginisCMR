# Wood Tiger Moth Mark Recapture

This package collects the functions, data, and analyses from the Capture Mark Recapture study on Wood Tiger Moths *Arctia plantaginis*, carried out in Keljonkangas (Central Finland) during the summers of 2014 and 2015.

The package includes individual-based data on:

- **Capture histories** (to estimate survival) and **locations** (to estimate dispersal)
- **Phenotype**: sex, colour morph, and morphology (wing areas).
- **Genetics**: relatedness among individuals

## Study Design

We carried out the study was carried out between May and August of 2014 and 2015 along a predefined 800m transect. Every day, between 2 and 4 people slowly walked the transect and searchd for moths along the transect as well as on its sides making incursions into the side fields and forests up to 200m away from the transect.

We caught (or tried to catch) every individual sighted. We marked all unmarked individuals with four colour markings in 4 possible locations of their underwing. For all individuals, we recorded:

- Identity
- Sex
- Colouration (male morph, female colour, melanine markings)
- Position (GPS coordinates)
- Behaviour
- Current weather

## Package Usage

If you are a member of the project but that just wants to see how the analyses were made or reports of the results, you will find those under the Articles tab. If you want to use the package or need to contribute data, documentation, or code you must clone this package and install it in your computer.

This project is version controlled via `git` hosted in [GitLab](https://gitlab.com/) to enable sharing and synchronization among collaborators.

In order to have a copy of this package in your computer, you must:

- Have access rights to the GitLab project repository at https://gitlab.com/lopezsepulcre/plantaginisCMR

- Have `git` installed and set up in your computer. If you don't, go to  https://git-scm.com to download it.

- If you never used git collaboratively, you will probably have to create an SSH key as described [here](http://happygitwithr.com/ssh-keys.html).


To clone the full project repository, type in Terminal:

```{bash, echo = TRUE, eval = FALSE}
git clone git@gitlab.com:lopezsepulcre/plantaginisCMR.git
```
 
To contribute, you can then open the package with RStudio by clicking on the `plantaginisCMR.Rproj` file. Remember to branch, commit and request merges with your code.

To use the package, simply type in RStudio console:

```{r, echo = TRUE, eval = FALSE}
library(plantaginisCMR)
```
