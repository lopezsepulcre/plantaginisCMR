par(new=FALSE)
plot_locations(captures=subset(mothCaptures,Morph=='W' & Year==2015))
par(new=TRUE)
plot_locations(captures=subset(mothCaptures,Morph=='Y' & Year==2015),pch=16)
par(new=TRUE)
plot_locations(captures=subset(mothCaptures,Morph=='F' & Year==2015),col='red',pch=4)
legend(25.6955,62.195,col=c('red','black','black'),pch=c(4,1,16),legend=c('F','W','Y'),title=2015)

##
