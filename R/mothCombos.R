#' Create Individual Moth Marks
#'
#' Creates individual IDs given the Wood Tiger Moth marking scheme developed by Swanne Gordon with two marks on the left underwings and two marks on the right underwings.
#'
#' @param morphs Vector of codes for the morphs or unique types to be marked (e.g. \code{W},\code{Y},\code{F}). Marks can be repeated across morphs because they are visually distinguishable.
#' @param positions Vector of numbers corresponding to the positions used to mark one side of the wing (positions are symmetric on both sides of the body). Normally, \code{c(1,2,3,4)}.
#' @param colours Vector of codes for the colours used to mark the underwings (e.g. \code{c('R','S')} for red and silver)
#' @return A character vector of all possible unique moth IDs of the format \code{morph-leftID-rightID} (e.g. \code{W-1G2G-1S2R}).
#'
mothCombos<-function(morphs,positions,colours){
rm(list=ls()) # CLEAR ALL
library(gtools)

C<-permutations(length(colours),2,colours,repeats.allowed=T)
P<-combinations(length(positions),2,positions,repeats.allowed=F)

COMBOS<-character()

for (s in 1:length(morphs)){
  for (pr in 1:dim(P)[1]){
  for (pl in 1:dim(P)[1]){
    for (cr in 1:dim(C)[1]){
    for (cl in 1:dim(C)[1]){

      COMBOS<-append(COMBOS,paste(morphs[s],'-',P[pr,1],C[cr,1],P[pr,2],C[cr,2],'-',P[pl,1],C[cl,1],P[pl,2],C[cl,2],sep=""))

    }}
  }}
}

return(COMBOS)
}
