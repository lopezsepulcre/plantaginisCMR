---
title: "Dispersal Modeling"
author: "Andrés López-Sepulcre"
date: "2/11/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(plantaginisCMR)
```

### A capture-mark-recapture model of dispersal

We want to estimate the survival, recruitment, and dispersal of the two morphs of *Arctia plangaginis* from spatially-explicit capture-mark-recapture data.

**Movement**

First, we can define a movement function that determines the new coordinates $(x_t,y_t)$ given some origin coordinates $(x_{t-1},y_{t-1})$, an angle of dispersal $\theta$, and a dispersal distance $\delta$

$$
x_t = x_{t-1} + (\delta \cdot cos(\theta)) \\
y_t = y_{t-1} + (\delta \cdot sin(\theta))
$$
If we consider the probability of survival $\phi$ and capture $p$, we can define a likelihood function of observing the coordinates $(\mathbf{x_t}, \mathbf{y_t})$

$$
L(\mathbf{x_t}) = x_{t-1} + (\delta \cdot cos(\theta)) \cdot \phi \cdot p \\
L(\mathbf{y_t}) = y_{t-1} + (\delta \cdot sin(\theta)) \cdot \phi \cdot p
$$
The joint likelihood will then be

$$
L(\mathbf{x_t}, \mathbf{y_t}) = L(\mathbf{x_t}) \cdot L(\mathbf{y_t})
$$
**Detection**

The probability of survival in transect surveys is not going to be the same throughout the sampling area. It is going to be a decreasing function of the distance to the transect. We can, for example, use a half-normal function, which takes parameter $\sigma$ as the spread of the function (the lower $\sigma$, the faster the drop in detectability), and is a function of the distance to the transect $d$.

$$
p = e^{-\frac{d^2}{2 \sigma^2}}
$$
where $d \geq0$.

Let's graph this function to get an idea, using our function `halfnorm`.

```{r}
xx <- seq(0,10, 0.1)
p1 <- halfnorm(x = xx, sigma = 1)
p2 <- halfnorm(x = xx, sigma = 5)

plot(p1 ~ xx, type = 'l', ylab = 'capture probability (p)', xlab = 'distance (d)')
lines(p2 ~ xx, lty =2)
legend(x = 8, y = 1, legend = c(expression(paste(sigma, ' = 1')), expression(paste(sigma, ' = 5'))), lty = c(1,2), bty = 'n')

```

However, this assumes that the capture probability along the transect is 1, which is not the case, this can be easily fixed by defining a scaling parameter defining the capture probability at origin (transect) $p_0$. The function thus becomes:

$$
p_d = p_0 \cdot e^{-\frac{d^2}{2 \sigma^2}}
$$

**Calculating distance to the transect**

Now that we have the likelihood functions, the one tricky thing left is to calculate the distance to the transect. We choose to calculate the shortest (minimum) distance between the coordinates of the captured moth, and the transect. 

To calculate this, we developed the function `distoset`. For example, if we want to know the distance of a given male with known coordinates, to the closest of all females, we can do:

```{r}
female.coordinates<-cbind(lon=c(25.70650,25.70647,25.70646), lat=c(62.19506,62.19506,62.19523))

closest_fem <-distoset(lonlat=c(25.706572,62.19500),target=female.coordinates)
closest_fem
```

The closest female to that malke is `r round(closest_fem,2)`m away.

We can do the same for the closest distance to a transect given aknown set of coordinates along that transect. For our capture recapture study, the coordinates of the transect can be accessed in `mothTransect`.

Because it will be computationally very intensive to use this function repeatedly during the Bayesian model estimation, the most efficient way is to imput, into the MCMC, a matrix of the distanes from every possible coordinate (to the GPS precision) to the transect, so that the MCMC algorithm just needs to access the right one rather than recalculate all the time.

Let's create that matrix.

```{r}

```


